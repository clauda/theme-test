window.template = window.template || {};
var ww = document.body.clientWidth;
var maxHeight    = $(window).height(); 

template.Application = {
	menu: function () {
		$(window).bind('resize orientationchange', function() {
			ww = document.body.clientWidth;
			adjustMenu();
		});
		var adjustMenu = function() {
			if (ww < 768) {
				$(".toggle-menu").css("display", "block");
				if (!$(".toggle-menu").hasClass("active")) {
					$(".nav").hide();
				} else {
					$(".nav").show();
				}
				$(".nav li").unbind('mouseenter mouseleave');
				$(".nav li a.parent").unbind('click').bind('click', function(e) {
					// must be attached to anchor element to prevent bubbling
					e.preventDefault();
					$(this).parent("li").toggleClass("hover");
				});
			} 
			else if (ww >= 768) {
				$(".toggle-menu").css("display", "none");
				$(".nav").show();
				$(".nav li").removeClass("hover");
				$(".nav li a").unbind('click');
				$(".nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
					// must be attached to li so that mouseleave is not triggered when hover over submenu
					$(this).toggleClass('hover');
				});
			}
		}
		$(".nav li a").each(function() {
			if ($(this).next().length > 0) {
				$(this).addClass("parent");
			};
		})
		
		$(".toggle-menu").click(function(e) {
			e.preventDefault();
			$(this).toggleClass("active");
			$(".nav").toggle();
		});
		$(".toggle-link").click(function(e) {
			e.preventDefault();
			$(".toggle-menu").toggleClass("active");
			$(".nav").toggle();
		});
		adjustMenu();
	},
	menuActive: function() {
		var url = window.location.pathname, 
		urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
		// now grab every link from the navigation
		if( url === "/") {
			$('.nav a').eq(0).addClass('actived');
		} else {
			$('.nav a').each(function(){
				// and test its normalized href against the url pathname regexp
				if(urlRegExp.test(this.href.replace(/\/$/,''))){
					$(this).addClass('actived');
				}
			});
		}
	},
	gallery: function() {
		var indexActual = "0";
		var totalLength = $('.image-hover-effect').length;
		$(document).on( 'click', '.image-hover-effect', function() {
			var indexActual  = $('.image-hover-effect').index(this);

			var bigImage     = $(this).attr('data-image');
			var mobileImage  = $(this).attr('data-image-mobile');
			var contentImage = $(this).find('.image-description').html();
			var galleryHtml  = "<div id='gallerymodal' class='large-12 columns animated fadeInDown' style='@media screen and (max-width: 768px) {height:"+maxHeight+"px;'>";
					galleryHtml += "<div class='header-gallery'><a href='#' class='close-gallery-modal'>fechar</a></div>";
					galleryHtml += "<div class='image-container'><img src='"+bigImage+"' class='hide-for-small' /><img src='"+mobileImage+"' class='show-for-small' /><a href='#' class='next-photo replacement hide-for-small animated-all'>next</a><a href='#' class='prev-photo replacement hide-for-small animated-all'>prev</a></div>";
					galleryHtml += "<div class='title-gallery'>"+contentImage+"</div>";
					galleryHtml += "</div>";

			$('#gallerymodal').remove();
			$(this).parent().parent().append(galleryHtml+"<div class='wk-overlay animated fadeIn'></div>");
	
			$(document).on('click', '.next-photo', function (e) {
				
				indexActual ++;

				if(indexActual >= totalLength){
					indexActual = 0;
				}

				var nextPosition = indexActual;

				var bigImage     = $('.image-hover-effect').eq(nextPosition).attr('data-image');
				var mobileImage  = $('.image-hover-effect').eq(nextPosition).attr('data-image-mobile');
				var contentImage = $('.image-hover-effect').eq(nextPosition).find('.image-description').html();

				$("#gallerymodal .image-container").html("<img src='"+bigImage+"' class='hide-for-small' /><img src='"+mobileImage+"' class='show-for-small' /><a href='#' class='next-photo replacement hide-for-small animated-all'>next</a><a href='#' class='prev-photo replacement hide-for-small animated-all'>prev</a>")
				$("#gallerymodal .title-gallery").html("<div class='title-gallery'>"+contentImage+"</div>");
				e.preventDefault();
			});

			$(document).on('click', '.prev-photo', function (e) {

				indexActual --;

				if(indexActual >= totalLength){
					indexActual = 0;
				}
				
				var prevPosition = indexActual;

				var bigImage     = $('.image-hover-effect').eq(prevPosition).attr('data-image');
				var mobileImage  = $('.image-hover-effect').eq(prevPosition).attr('data-image-mobile');
				var contentImage = $('.image-hover-effect').eq(prevPosition).find('.image-description').html();

				$("#gallerymodal .image-container").html("<img src='"+bigImage+"' class='hide-for-small' /><img src='"+mobileImage+"' class='show-for-small' /><a href='#' class='next-photo replacement hide-for-small animated-all'>next</a><a href='#' class='prev-photo replacement hide-for-small animated-all'>prev</a>")
				$("#gallerymodal .title-gallery").html("<div class='title-gallery'>"+contentImage+"</div>");
				e.preventDefault();
			});
		});

		$(document).on('click', '.close-gallery-modal, .wk-overlay', function() {
			$('#gallerymodal, .wk-overlay').remove();
			return false;
		});
	},
	init: function () {
		this.menu();
		this.gallery();
		this.menuActive();
		this.carrousel();
	}
}

$(document).ready(function () {
	template.Application.init();
});