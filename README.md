## Contents Conversions

#### `contents.json`

- keys dont repeat 
- keys should use underline case
- use prefix helps to keep namespace, like home_title, about_title, etc.
- images should have image_ prefix and _{size} sufix, like image_banner_640x240

#### Ordinary Keys

Some keys represents data specifics:

- company_brand: company name
- company_address: damn
- company_phone
- maps_address: to google maps